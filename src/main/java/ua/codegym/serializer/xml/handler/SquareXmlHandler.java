package ua.codegym.serializer.xml.handler;

import ua.codegym.serializer.Serializer;
import ua.codegym.serializer.shape.Shape;
import ua.codegym.serializer.shape.Square;

import java.io.IOException;
import java.io.OutputStream;

public class SquareXmlHandler extends AbstractSerializer implements Serializer {

  @Override
  public void serialize(Shape shape, OutputStream os) {
  if (!(shape instanceof Square)) {
    throw new RuntimeException("Incorrect shape" + shape) ;
  }

  Square square = (Square) shape;

  String template = "<square x=\"%d\" y=\"%d\" side=\"%d\"></square>";

  String output = String.format(template,square.getX(),square.getY(),square.getSide());

    write(output,os);
  }
}