package ua.codegym.serializer.xml.handler;

import ua.codegym.serializer.Serializer;

import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;

public abstract class AbstractSerializer implements Serializer {

    protected void write(String output, OutputStream os)
    {
      try{
        os.write(output.getBytes());

      }
      catch (IOException e)
      {
        throw new RuntimeException("Cannot write" + output);
      }
    }

}
